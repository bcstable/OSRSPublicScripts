package bcstable.scripts.chickenKiller;

/**
 * Created by Brett on 3/27/2017.
 */
public enum LootState
{
    FEATHERSBONES,
    FEATHERS,
    BONES
}
