package bcstable.scripts;

import org.powerbot.script.*;
import org.powerbot.script.rt4.*;
import org.powerbot.script.rt4.ClientContext;

import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/27/2017.
 */
public abstract class GenericQuestTask extends Task<ClientContext>
{
    protected HashMap<Integer, int[]> bounds = new HashMap<Integer, int[]>();

    private final int RED_TEXT = 16711680;
    private final int GREEN_TEXT = 65280;

    public GenericQuestTask(ClientContext ctx)
    {
        super(ctx);
    }

    protected GameObject findNextBoundary(final int... ids)
    {
        final GameObject boundary = ctx.objects.select().id(ids).nearest().poll();

        return boundary;

    }

    protected void clearBoundary(final GameObject boundary, final int[] bounds)//final Locatable locatable, final int[] bounds)
    {
        if(boundary.valid())
        {
            if (!boundary.inViewport())
            {
                ctx.movement.step(boundary);
                Condition.wait(new Callable<Boolean>()
                {
                    @Override
                    public Boolean call() throws Exception
                    {
                        return boundary.inViewport();
                    }
                }, 500, 30);
            }
            boundary.bounds(bounds);
            boundary.interact("Open", boundary.name());
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return !boundary.valid();
                }
            }, 350, 16);
        }
    }

    protected boolean isReachable(Locatable l)
    {
        Tile t= l.tile();
        ctx.objects.select().at(t);
        //filters to only include gates
        ctx.objects.select(new Filter<GameObject>()
        {
            @Override
            public boolean accept(GameObject gameObject)
            {
                return gameObject.type() == GameObject.Type.BOUNDARY;
            }
        });
        if (ctx.objects.isEmpty() && t.matrix(ctx).reachable())
        {
            return true;
        }
        return false;
    }

    protected boolean itemsInInventory(int id, int amount)
    {
        if(ctx.inventory.select().id(id).count() < amount)
        {
            return false;
        }
        return true;
    }

    protected boolean questFinished(int component)
    {
        ctx.game.tab(Game.Tab.QUESTS);

        if(ctx.widgets.widget(399).component(7).component(component).textColor() == GREEN_TEXT)
        {
            return true;
        }
        return false;
    }


    protected void getInViewPort(final Npc npc) //will probably have to be abstract.
    {
        if(!npc.inViewport())
        {
            ctx.movement.step(npc);
            ctx.camera.turnTo(npc);
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return npc.inViewport();
                }
            },1000, 30);
        }

    }



    protected void selectChatOptions(Npc npc, String[] chatOptions)
    {
        if(!ctx.chat.select().text(chatOptions).isEmpty())
        {
            final ChatOption option = ctx.chat.select().text(chatOptions).poll();
            if(option.select(true))
            {
                Condition.wait(new Callable<Boolean>()
                {
                    @Override
                    public Boolean call() throws Exception
                    {
                        return !option.valid();
                    }
                }, 500, 10);
            }
        }
    }

    protected void continueChatting(Npc npc)
    {
        if(ctx.chat.canContinue())
        {
            ctx.chat.clickContinue(true);
            Condition.sleep(Random.nextInt(750, 1000));
        }
    }

    protected void startChatting(Npc npc)
    {
        if(!ctx.chat.chatting())
        {
            if(npc.interact("Talk-to", npc.name()))
            {
                Condition.wait(new Callable<Boolean>()
                {
                    @Override
                    public Boolean call() throws Exception
                    {
                        return ctx.chat.chatting();
                    }
                }, 500,10);
            }
        }
    }

}
