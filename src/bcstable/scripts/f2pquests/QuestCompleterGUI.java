package bcstable.scripts.f2pquests;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Brett on 4/18/2017.
 */
public class QuestCompleterGUI extends JFrame
{


    private String req = "Required Items:\n";
    private String cooksAssistantReq = "1 x egg, 1 x pot of flour, 1 x bucket of milk\n";
    private String sheepShearerReq = "20 x ball of wool\n";
    private String doricsQuestReq = "6 x clay, 2 x iron ore 440, 4 x copper ore\n";
    private String goblinDiplomacyReq = "3 x goblin mail, 1 x orange dye, 1 x blue dye\n";
    private JTextArea requirements;

    private boolean cooksAssistant;
    private boolean sheepShearer;
    private boolean doricsQuest;
    private boolean goblinDiplomacy;

    private boolean done = false;
    public QuestCompleterGUI() //TODO needs reformatting when script is finished, new layoutManager etc
    {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new GridBagLayout());
        setSize(500, 400);
        setTitle("SaiyanKunt's F2P Quest Completer");
        JLabel label = new JLabel(" Select the quests to complete and ensure the requirements are met.");
        final JCheckBox cooksAssistantCB = new JCheckBox("Cook's Assistant", false);
        final JCheckBox sheepShearerCB = new JCheckBox("Sheep Shearer", false);
        final JCheckBox doricsQuestCB = new JCheckBox("Doric's Quest", false);
        final JCheckBox goblinDiplomacyCB = new JCheckBox("Goblin Diplomacy", false);
        final JButton button = new JButton("Start");
        requirements = new JTextArea();
        final JScrollPane requirementsPane = new JScrollPane(requirements, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        requirements.setFont(new Font("Tahoma", Font.PLAIN, 12));
        requirementsPane.setPreferredSize(new Dimension(300, 200));
        requirements.setEditable(false);
        requirements.setVisible(true);
        requirements.setLineWrap(true);
        requirements.setText(req);

        GridBagConstraints gc = new GridBagConstraints();
        gc.anchor = GridBagConstraints.LINE_START;
        gc.weightx = 1; gc.weighty = 1;
        gc.gridx = 0; gc.gridy = 1;
        add(cooksAssistantCB, gc);
        gc.gridx = 0; gc.gridy = 2;
        add(sheepShearerCB, gc);
        gc.gridx = 0; gc.gridy = 3;
        add(doricsQuestCB, gc);
        gc.gridx = 0; gc.gridy = 4;
        add(goblinDiplomacyCB, gc);

        gc.anchor = GridBagConstraints.FIRST_LINE_END;
        gc.gridx = 0;gc.gridy =2; gc.gridheight=4;
        add(requirementsPane, gc);
        gc.anchor = GridBagConstraints.CENTER;
        gc.gridheight=1;
        gc.gridx = 0; gc.gridy = 0;
        add(label, gc);
        gc.gridx =0; gc.gridy = 5;
        add(button, gc);

        addActionListener(cooksAssistantCB, cooksAssistantReq);
        addActionListener(sheepShearerCB, sheepShearerReq);
        addActionListener(doricsQuestCB, doricsQuestReq);
        addActionListener(goblinDiplomacyCB, goblinDiplomacyReq);

        button.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(!cooksAssistantCB.isSelected() && !sheepShearerCB.isSelected() && !doricsQuestCB.isSelected() && !goblinDiplomacyCB.isSelected())
                {
                    System.out.print("Please select at least one task");
                    return;
                }
                cooksAssistant = cooksAssistantCB.isSelected();
                sheepShearer = sheepShearerCB.isSelected();
                doricsQuest = doricsQuestCB.isSelected();
                goblinDiplomacy = goblinDiplomacyCB.isSelected();

                dispose();
                done = true;
            }
        });

    }

    private void addActionListener(final JCheckBox cb, final String text)
    {
        cb.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(cb.isSelected())
                {
                    requirements.setText(requirements.getText() + text);
                }
                else
                {
                    requirements.setText(requirements.getText().replace(text, ""));
                }

            }
        });
    }

    public boolean done()
    {
        return done;
    }

    public boolean cooksAssistant()
    {
        return cooksAssistant;
    }
    public boolean sheepShearer()
    {
        return sheepShearer;
    }
    public boolean doricsQuest()
    {
        return doricsQuest;
    }
    public boolean goblinDiplomacy()
    {
        return goblinDiplomacy;
    }

}
