package bcstable.scripts.f2pquests;

import bcstable.scripts.GenericQuestTask;
import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.rt4.Npc;

import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/30/2017.
 */
public class DoricsQuest extends GenericQuestTask
{
    private final int COMPONENT_ID = 3;
    private final int CLAY_ID = 434;
    private final int IRON_ORE_ID = 440;
    private final int COPPER_ORE_ID = 436;
    private final int DORIC_ID = 3893;
    private final int CLOSED_DOOR_ID = 1535;
    private final String[] CHAT_OPTIONS = {"I wanted to use your anvils.", "Yes, I will get you the materials", "Certainly, I'll be right back!"};
    private final int[] CLOSED_DOOR_BOUNDS = {116, 124, -196, -24, -104, -24};

    public DoricsQuest(ClientContext ctx)
    {
        super(ctx);
        bounds.put(CLOSED_DOOR_ID, CLOSED_DOOR_BOUNDS);
    }

    @Override
    public String getName()
    {
        return null;
    }

    @Override
    public boolean activate()
    {
        if(!questFinished(COMPONENT_ID) && itemsInInventory(CLAY_ID, 6) && itemsInInventory(IRON_ORE_ID, 2) && itemsInInventory(COPPER_ORE_ID, 4)
                && ctx.npcs.select().id(DORIC_ID).nearest().poll().valid())
        {
            return true;
        }
        return false;
    }

    @Override
    public void execute()
    {
        Npc doric = ctx.npcs.select().id(DORIC_ID).nearest().poll();
        getInViewPort(doric);
        if(!isReachable(doric))
        {
            return;
        }
        startChatting(doric);
        continueChatting(doric);
        selectChatOptions(doric, CHAT_OPTIONS);
        if(questFinished(COMPONENT_ID))
        {
            completed = true;
        }

    }

    @Override
    protected void getInViewPort(final Npc npc) //will probably have to be abstract.
    {
        if(!isReachable(npc))
        {
            GameObject boundary = findNextBoundary(CLOSED_DOOR_ID);
            if(boundary.valid())
            {
                clearBoundary(boundary, bounds.get(boundary.id()));
            }
        }
        else if(!npc.inViewport())
        {
            ctx.movement.step(npc);
            ctx.camera.turnTo(npc);
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return npc.inViewport();
                }
            },1000, 30);
        }

    }
}
