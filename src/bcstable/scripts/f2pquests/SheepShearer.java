package bcstable.scripts.f2pquests;

import bcstable.scripts.GenericQuestTask;
import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.rt4.Npc;

import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/30/2017.
 */
public class SheepShearer extends GenericQuestTask
{

    private final int BALL_WOOL_ID =1759;
    private final int FRED_ID = 732;//
    private final String[] CHAT_OPTIONS = {"I'm looking for a quest.", "Yes okay. I can do that.", "Of course!", "I'm back!", "I'm something of an expert actually!"};
    private final int COMPONENT_ID = 14;

    private final int CLOSED_FREDS_GATE_ID = 12987;
    private final int CLOSED_DOOR_ID = 13001;
    final int[] FREDS_GATE_BOUNDS = {-84, 72, -116, -36, 104, 116};
    final int[] DOOR_BOUNDS = {24, 108, -220, -24, 112, 124};

    public SheepShearer(ClientContext ctx)
    {
        super(ctx);
        bounds.put(CLOSED_DOOR_ID, DOOR_BOUNDS);
        bounds.put(CLOSED_FREDS_GATE_ID, FREDS_GATE_BOUNDS);
    }


    @Override
    public String getName()
    {
        return null;
    }

    @Override
    public boolean activate()
    {
        if( !questFinished(COMPONENT_ID) && itemsInInventory(BALL_WOOL_ID, 20) && ctx.npcs.select().id(FRED_ID).nearest().poll().valid())
        {
            return true;
        }
        return false;
    }

    @Override
    public void execute()
    {
        final Npc fredTheFarmer = ctx.npcs.select().id(FRED_ID).nearest().poll();
        getInViewPort(fredTheFarmer);
        if(!isReachable(fredTheFarmer))
        {
            return;
        }
        startChatting(fredTheFarmer);
        continueChatting(fredTheFarmer);
        selectChatOptions(fredTheFarmer, CHAT_OPTIONS);
        if(questFinished(COMPONENT_ID))
        {
            completed = true;
        }

    }


    @Override
    protected void getInViewPort(final Npc npc) //will probably have to be abstract.
    {
        if(!isReachable(npc))
        {
            GameObject boundary = findNextBoundary(CLOSED_FREDS_GATE_ID, CLOSED_DOOR_ID);
            if(boundary.valid())
            {
                clearBoundary(boundary, bounds.get(boundary.id()));
            }
        }
        else if(!npc.inViewport())
        {
            ctx.movement.step(npc);
            ctx.camera.turnTo(npc);
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return npc.inViewport();
                }
            },1000, 30);
        }

    }
}
