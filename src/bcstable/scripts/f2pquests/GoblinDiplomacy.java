package bcstable.scripts.f2pquests;

import bcstable.scripts.GenericQuestTask;
import org.powerbot.script.Condition;
import org.powerbot.script.rt4.*;

import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/30/2017.
 */
public class GoblinDiplomacy extends GenericQuestTask
{

    private final int COMPONENT_ID = 4;
    private final String[] CHAT_OPTIONS = {"Do you want me to pick an armour colour for you?", "What about a different colour?", "I have some orange armour here", "I have some blue armour here", "I have some brown armour here", "No, he doesn't look fat"};

    private final int BLUE_DYE_ID = 1767;
    private final int ORANGE_DYE_ID = 1769;
    private final int GOBLIN_MAIL_ID = 288;
    private final int BENTNOZE_ID = 669;
    private final int CLOSED_DOOR_ID = 12446;
    private final int[] CLOSED_DOOR_BOUNDS = {52, 128, -204, -64, 116, 124};
    private final int BLUE_MAIL_ID = 287;
    private final int ORANGE_MAIL_ID = 286;


    public GoblinDiplomacy(ClientContext ctx)
    {
        super(ctx);
        bounds.put(CLOSED_DOOR_ID, CLOSED_DOOR_BOUNDS);
    }


    @Override
    public String getName()
    {
        return null;
    }

    @Override
    public boolean activate()
    {
        if(!questFinished(COMPONENT_ID) && ctx.npcs.select().id(BENTNOZE_ID).nearest().poll().valid() && (dyeAndMailInInventory() || mailAlreadyDyed()))
        {
            return true;
        }
        return false;
        //goblin village north fally
        //blue dye ID1767, orange dye 1769, 3 goblin mail 288 (possibly make gob mail optional) << second release?

    }

    private boolean mailAlreadyDyed()
    {
        if (itemsInInventory(GOBLIN_MAIL_ID, 1) && itemsInInventory(ORANGE_MAIL_ID, 1) && itemsInInventory(BLUE_MAIL_ID, 1))
        {
            return true;
        }
        return false;
    }

    private boolean dyeAndMailInInventory()
    {
        if(itemsInInventory(GOBLIN_MAIL_ID, 3) && itemsInInventory(BLUE_DYE_ID, 1) && itemsInInventory(ORANGE_DYE_ID, 1))
        {
            return true;
        }
        return false;
    }

    private void dyeGoblinMails()
    {
        if(itemsInInventory(ORANGE_MAIL_ID, 1) && itemsInInventory(BLUE_MAIL_ID, 1))
        {
            return;
        }
        if(itemsInInventory(GOBLIN_MAIL_ID, 1) && itemsInInventory(BLUE_DYE_ID, 1))
        {
            ctx.game.tab(Game.Tab.INVENTORY);

            ctx.inventory.select().id(BLUE_DYE_ID).poll().interact("Use");
            ctx.inventory.select().id(GOBLIN_MAIL_ID).poll().click();
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return itemsInInventory(BLUE_MAIL_ID, 1);
                }
            },500, 10);
        }
        if(itemsInInventory(GOBLIN_MAIL_ID, 1) && itemsInInventory(ORANGE_DYE_ID,1))
        {
            ctx.inventory.select().id(ORANGE_DYE_ID).poll().interact("Use");
            ctx.inventory.select().id(GOBLIN_MAIL_ID).poll().click();
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return itemsInInventory(ORANGE_MAIL_ID, 1);
                }
            },500, 10);
        }
    }

    @Override
    public void execute()
    {
        //at least one double door 12446 and 12448

        Npc bentnoze = ctx.npcs.select().id(BENTNOZE_ID).nearest().poll();
        if(!ctx.chat.chatting())
        {
            dyeGoblinMails();
            getInViewPort(bentnoze);
            if(!isReachable(bentnoze)) //sometimes the door is not clicked correctly
            {
                return;
            }
            startChatting(bentnoze);
        }

        continueChatting(bentnoze);
        selectChatOptions(bentnoze, CHAT_OPTIONS);

        if(questFinished(COMPONENT_ID))
        {
            completed = true;
        }

    }

    @Override
    protected void getInViewPort(final Npc npc) //will probably have to be abstract.
    {
        if(!isReachable(npc))
        {
            GameObject boundary = findNextBoundary(CLOSED_DOOR_ID);
            if(boundary.valid())
            {
                clearBoundary(boundary, bounds.get(boundary.id()));
            }
        }
        else if(!npc.inViewport())
        {
            ctx.movement.step(npc);
            ctx.camera.turnTo(npc);
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return npc.inViewport();
                }
            },1000, 30);
        }

    }
}
