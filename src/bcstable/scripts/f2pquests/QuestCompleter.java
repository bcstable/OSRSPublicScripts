package bcstable.scripts.f2pquests;

import bcstable.scripts.Task;
import org.powerbot.script.*;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.PollingScript;
import org.powerbot.script.rt4.Game;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/30/2017.
 */

@Script.Manifest(
        name = "Quest Completer", properties = "author=Brett; topic=; client=4;",
        description = "A basic script that kills chickens, collects feathers and bones after 5 kills and buries bones when the inventory is full.")

public class QuestCompleter extends PollingScript<ClientContext>
{
    LinkedList<Task> taskList = new LinkedList<Task>();
    HashMap<Integer, Integer> questStatus = new HashMap<Integer, Integer>();

    public void start()
    {
        final QuestCompleterGUI gui = new QuestCompleterGUI();
        gui.setVisible(true);
        Condition.wait(new Callable<Boolean>()
        {
            @Override
            public Boolean call() throws Exception
            {
                return gui.done() || !gui.isVisible();
            }
        }, 1000, 60);
        if(!gui.done())
        {
            ctx.controller.stop();
        }

        ctx.game.tab(Game.Tab.QUESTS);

        if(gui.cooksAssistant()) taskList.add(new CooksAssistant(ctx));
        if(gui.sheepShearer()) taskList.add(new SheepShearer(ctx));
        if(gui.goblinDiplomacy()) taskList.add(new GoblinDiplomacy(ctx));
        if(gui.doricsQuest()) taskList.add(new DoricsQuest(ctx));


        //check all finished quests
        //cache this, update it every time the current quest finishes
        //maybe have a hash map??

    }

    Task currentTask;
    public void poll()
    {

        ctx.camera.pitch(true);
        if(ctx.movement.energyLevel() > 25)
        {
            ctx.movement.running(true);
        }

        boolean noActiveTasks = true;
        for(Task task : taskList)
        {
            if(currentTask != null)
            {
                task = currentTask;
            }
            if(task.activate() || task == currentTask)
            {
                noActiveTasks = false;
                currentTask = task;
                task.execute();
                if(task.completed())
                {
                    currentTask = null;
                    taskList.remove(task);
                }
            }
        }
        if(noActiveTasks||taskList.isEmpty())
        {
            System.out.println("No active task found. Stopping script");
            ctx.controller.stop();
        }
    }
}
