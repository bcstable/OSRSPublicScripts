package bcstable.scripts.f2pquests;

import bcstable.scripts.GenericQuestTask;
import org.powerbot.script.Condition;
import org.powerbot.script.rt4.Npc;
import org.powerbot.script.rt4.*;

import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/30/2017.
 */
public class CooksAssistant extends GenericQuestTask
{
    //1944, bucket of milk 1927, pot of flour 1933
    private final int EGG_ID = 1944;
    private final int BUCKET_MILK_ID = 1927;
    private final int POT_FLOUR_ID = 1933;
    private final int COOK_ID = 4626;
    private final String[] CHAT_OPTIONS = {"What's wrong?", "I'm always happy to help a cook in distress.", "Actually, I know where to find this stuff."};
    private final int COMPONENT_ID = 1;

    public CooksAssistant(ClientContext ctx)
    {
        super(ctx);
    }

    @Override
    public String getName()
    {
        return null;
    }

    @Override
    public boolean activate()
    {
        if((!questFinished(COMPONENT_ID) && itemsInInventory(EGG_ID, 1) && itemsInInventory(BUCKET_MILK_ID, 1) && itemsInInventory(POT_FLOUR_ID, 1)
                && isReachable(ctx.npcs.select().id(COOK_ID).nearest().poll())))
        {
            return true;
        }
        return false;
    }

    @Override
    public void execute()
    {
        final Npc cook = ctx.npcs.select().id(COOK_ID).nearest().poll();
        getInViewPort(cook);
        startChatting(cook);
        continueChatting(cook);
        selectChatOptions(cook, CHAT_OPTIONS);
        if(questFinished(COMPONENT_ID))
        {
            completed = true;
        }
    }


    @Override
    protected void getInViewPort(final Npc npc) //will probably have to be abstract.
    {
        if(!npc.inViewport())
        {
            ctx.movement.step(npc);
            ctx.camera.turnTo(npc);
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return npc.inViewport();
                }
            },1000, 30);
        }

    }

}
