package bcstable.scripts;

import org.powerbot.script.ClientAccessor;
import org.powerbot.script.ClientContext;

/**
 * Created by Brett on 3/27/2017.
 */
public abstract class Task<C extends ClientContext> extends ClientAccessor<C>
{
    public Task(C ctx)
    {
        super(ctx);
    }
    protected boolean failed = false;
    protected boolean completed = false;
    public abstract boolean activate();

    public abstract void execute();

    public boolean completed() { return completed; }

    public boolean failed()
    {
        return failed;
    }

    public abstract String getName();


}
