package bcstable.scripts.practiceScriptChopAndDrop;

import org.powerbot.script.*;
import org.powerbot.script.rt4.*;
import org.powerbot.script.rt4.ClientContext;
import bcstable.scripts.Task;

import java.util.concurrent.Callable;

/**
 * Created by Brett on 3/27/2017.
 */
public class LightLogs extends Task<ClientContext>
{
    private Helper helper;
    private boolean fireFailed;
    public LightLogs(ClientContext ctx)
    {
        super(ctx);
        helper = new Helper(ctx);
    }

    @Override
    public void execute()
    {
        Item tinderbox = ctx.inventory.select().id(590, 591).peek();
        for(Item logs : ctx.inventory.id(helper.getLogIds()))
        {
            tinderbox.click();
            logs.click();
            Condition.sleep(Random.nextInt(280, 600));
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return !helper.alreadyActive();
                }
            }, 150, 70);
            Condition.sleep();

            if (fireFailed)
            {
                ctx.movement.step(ctx.players.local().tile().derive(Random.nextInt(-2,2), Random.nextInt(-2, 2)));
                Condition.sleep(Random.nextInt(600, 12));
                Condition.wait(new Callable<Boolean>()
                {
                    @Override
                    public Boolean call() throws Exception {
                        return !helper.alreadyActive();
                    }
                }, 500, 13);
            }
            fireFailed = false;
        }

    }

    void setFireFailed(boolean b)
    {
        fireFailed = b;
    }

    @Override
    public boolean activate()
    {
        if(helper.inventoryFull() && logsInInventory() && !helper.alreadyActive())
        {
            return true;
        }
        return false;
    }

    @Override
    public String getName()
    {
        return "LightLogs";
    }

    public boolean logsInInventory()
    {
        return ctx.inventory.select().id(helper.getLogIds()).count() > 0;
    }

}
