package bcstable.scripts.practiceScriptChopAndDrop;

import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.rt4.ClientContext;
import bcstable.scripts.Task;

import java.util.ArrayList;
import java.util.List;

@Script.Manifest(
        name = "Chop and Light", properties = "author=Brett; topic=1296208; client=4;",
        description = "A basic script that chops trees until its inventory is full, then lights the logs."
)
public class ChopAndDrop extends PollingScript<ClientContext> implements MessageListener
{
    private List<Task> taskList = new ArrayList<Task>();
    ChopTrees chop;
    LightLogs light;

    @Override
    public void start()
    {
        taskList.add(chop = new ChopTrees(ctx));
        taskList.add(light = new LightLogs(ctx));
    }

    @Override
    public void poll()
    {
        for(Task task : taskList)
        {
            log.info("Checking activate for task: " + task.getName());
            if(task.activate())
            {
                log.info(task.getName() + " activated. Executing task.");
                task.execute();
            }
        }

    }

    @Override
    public void messaged(MessageEvent e)
    {
        final String msg = e.text().toLowerCase();
        if (msg.equals("you can't light a fire here."))
        {
            light.setFireFailed(true);
        }
    }
}