package bcstable.scripts.practiceScriptChopAndDrop;

import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Item;
import bcstable.scripts.Task;

/**
 * Created by Brett on 3/27/2017.
 */
public class DropLogs extends Task<ClientContext>
{
    private Helper helper;
    public DropLogs(ClientContext ctx)
    {
        super(ctx);
        helper = new Helper(ctx);
    }

    @Override
    public void execute()
    {
        for(final Item item : ctx.inventory.id(helper.getLogIds()))
        {

            item.interact("Drop");
;
        }
    }
    @Override
    public boolean activate()
    {
        return helper.inventoryFull() && logsInInventory();
    }

    @Override
    public String getName()
    {
        return "DropLogs";
    }

    public boolean logsInInventory()
    {
        if(ctx.inventory.select().id(helper.getLogIds()).count() > 0)
        {
            System.out.println("Logs found in inventory");
            return true;
        }
        System.out.println("No logs found in inventory");
        return false;
    }

}
