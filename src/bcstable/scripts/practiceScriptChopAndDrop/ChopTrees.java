package bcstable.scripts.practiceScriptChopAndDrop;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.*;
import org.powerbot.script.rt4.*;
import bcstable.scripts.Task;

import java.util.concurrent.Callable;


/**
 * Created by Brett on 3/27/2017.
 */
public class ChopTrees extends Task<ClientContext>
{
    private Helper helper;

    public ChopTrees(ClientContext ctx)
    {
        super(ctx);
        helper = new Helper(ctx);
    }

    @Override
    public boolean activate()
    {
        if (treesAround() && !helper.inventoryFull() && !helper.alreadyActive())
        {
            return true;
        }
        return false;
    }

    @Override
    public void execute()
    {
        BasicQuery<GameObject> trees = ctx.objects.select().id(helper.getTreeIds());
        GameObject tree = trees.nearest().peek();
        for(GameObject tempTree : trees.nearest())
        {
            if(isReachable(tempTree))
            {
                tree = tempTree;
                break;
            }
        }

        if(tree.inViewport())
        {
            tree.interact("Chop", "tree");
            Condition.sleep();
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return !helper.alreadyActive();
                }
            }, 300, 30);
        }
        else
        {
            ctx.movement.step(tree);
            ctx.camera.turnTo(tree);
            Condition.wait(new Callable<Boolean>()
            {
                @Override
                public Boolean call() throws Exception
                {
                    return !helper.alreadyActive();
                }
            }, 150, 20);
        }


    }

    @Override
    public String getName()
    {
        return "ChopTrees";
    }

    public boolean treesAround()
    {
        if(ctx.objects.select().id(helper.getTreeIds()).isEmpty())
        {
            System.out.println("No trees are around.");
            return false;
        }
        return true;
    }

    boolean isReachable(Locatable l)
    {
        //array of orthogonal tiles
        final Tile[] tiles =
                {
                l.tile().derive(-1, 0), l.tile().derive(1, 0),
                l.tile().derive(0, -1), l.tile().derive(0, 1)
        };
        for (Tile t : tiles)
        {
            //filters objects to only include those at 't'
            ctx.objects.select().at(t);
            //filters to only include boundaries
            ctx.objects.select(new Filter<GameObject>()
            {
                @Override
                public boolean accept(GameObject gameObject)
                {
                    return gameObject.type() == GameObject.Type.BOUNDARY;
                }
            });
            //checks there are no objects and t is reachable
            if (ctx.objects.isEmpty() && t.matrix(ctx).reachable())
            {
                return true;
            }
        }
        return false;
    }
}
