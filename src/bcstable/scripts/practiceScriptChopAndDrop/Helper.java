package bcstable.scripts.practiceScriptChopAndDrop;

import org.powerbot.script.rt4.ClientContext;

import java.util.HashMap;

/**
 * Created by Brett on 3/27/2017.
 */
public class Helper
{
    private int[] logIds = {1511, 1512};
    private int[] treeIds = {8173, 678, 1276, 1277, 1278, 1280, 1301, 1303, 1304, 1305};
    private ClientContext ctx;
    public Helper(ClientContext ctx)
    {
        this.ctx = ctx;
    }

    public boolean inventoryFull()
    {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        


        if(ctx.inventory.select().count() < 28)
        {
            return false;
        }
        return true;
    }

    public boolean alreadyActive()
    {
        if(ctx.players.local().animation() != -1 || ctx.players.local().inMotion())
        {
            return true;
        }
        return false;
    }

    public int[] getTreeIds()
    {
        return treeIds;
    }

    public int[] getLogIds()
    {
        return logIds;
    }
}
