package bcstable.scripts;

import org.powerbot.bot.rt4.client.Tile;

/**
 * Created by Brett on 4/6/2017.
 */
public class PathGenerator
{

    //find their starting point, figure out which quests they need to do, figure out best path

    //possible starting points
    //lumby (3222, 3219, 0)
    //fred farmer (3189, 3280, 0)
    //doric (2948, 3449, 0)
    //goblin (2958,3509, 0)

    //lumby to fred= public static final Tile[] path = {new Tile(3222, 3218, 0), new Tile(3225, 3218, 0), new Tile(3228, 3218, 0), new Tile(3231, 3218, 0), new Tile(3232, 3221, 0), new Tile(3232, 3224, 0), new Tile(3232, 3227, 0), new Tile(3232, 3230, 0), new Tile(3229, 3232, 0), new Tile(3226, 3234, 0), new Tile(3223, 3237, 0), new Tile(3221, 3240, 0), new Tile(3220, 3243, 0), new Tile(3219, 3246, 0), new Tile(3218, 3249, 0), new Tile(3218, 3252, 0), new Tile(3217, 3255, 0), new Tile(3214, 3258, 0), new Tile(3214, 3261, 0), new Tile(3216, 3264, 0), new Tile(3216, 3267, 0), new Tile(3216, 3270, 0), new Tile(3216, 3273, 0), new Tile(3215, 3276, 0), new Tile(3212, 3277, 0), new Tile(3209, 3278, 0), new Tile(3206, 3278, 0), new Tile(3203, 3278, 0), new Tile(3200, 3278, 0), new Tile(3197, 3279, 0), new Tile(3194, 3280, 0), new Tile(3191, 3280, 0)};
    //fred to doric = public static final Tile[] path = {new Tile(3189, 3280, 0), new Tile(3186, 3282, 0), new Tile(3183, 3282, 0), new Tile(3180, 3285, 0), new Tile(3177, 3286, 0), new Tile(3174, 3286, 0), new Tile(3171, 3286, 0), new Tile(3168, 3286, 0), new Tile(3165, 3286, 0), new Tile(3162, 3287, 0), new Tile(3159, 3290, 0), new Tile(3156, 3292, 0), new Tile(3153, 3292, 0), new Tile(3150, 3294, 0), new Tile(3147, 3295, 0), new Tile(3144, 3295, 0), new Tile(3141, 3295, 0), new Tile(3138, 3295, 0), new Tile(3135, 3298, 0), new Tile(3132, 3300, 0), new Tile(3129, 3303, 0), new Tile(3126, 3304, 0), new Tile(3123, 3305, 0), new Tile(3120, 3307, 0), new Tile(3119, 3310, 0), new Tile(3116, 3311, 0), new Tile(3114, 3314, 0), new Tile(3111, 3316, 0), new Tile(3108, 3317, 0), new Tile(3105, 3318, 0), new Tile(3102, 3318, 0), new Tile(3099, 3318, 0), new Tile(3096, 3319, 0), new Tile(3093, 3320, 0), new Tile(3090, 3320, 0), new Tile(3087, 3322, 0), new Tile(3084, 3325, 0), new Tile(3083, 3328, 0), new Tile(3081, 3331, 0), new Tile(3078, 3334, 0), new Tile(3075, 3337, 0), new Tile(3073, 3340, 0), new Tile(3073, 3343, 0), new Tile(3073, 3346, 0), new Tile(3072, 3349, 0), new Tile(3069, 3352, 0), new Tile(3067, 3355, 0), new Tile(3065, 3358, 0), new Tile(3065, 3361, 0), new Tile(3065, 3364, 0), new Tile(3066, 3367, 0), new Tile(3066, 3370, 0), new Tile(3066, 3373, 0), new Tile(3066, 3376, 0), new Tile(3066, 3379, 0), new Tile(3066, 3382, 0), new Tile(3066, 3385, 0), new Tile(3064, 3388, 0), new Tile(3061, 3390, 0), new Tile(3058, 3393, 0), new Tile(3056, 3396, 0), new Tile(3053, 3399, 0), new Tile(3050, 3401, 0), new Tile(3047, 3403, 0), new Tile(3044, 3404, 0), new Tile(3041, 3405, 0), new Tile(3038, 3407, 0), new Tile(3035, 3408, 0), new Tile(3032, 3408, 0), new Tile(3029, 3408, 0), new Tile(3026, 3411, 0), new Tile(3023, 3413, 0), new Tile(3020, 3414, 0), new Tile(3017, 3414, 0), new Tile(3014, 3416, 0), new Tile(3011, 3416, 0), new Tile(3008, 3418, 0), new Tile(3005, 3418, 0), new Tile(3002, 3418, 0), new Tile(2999, 3418, 0), new Tile(2996, 3418, 0), new Tile(2993, 3418, 0), new Tile(2990, 3418, 0), new Tile(2987, 3418, 0), new Tile(2984, 3420, 0), new Tile(2981, 3423, 0), new Tile(2978, 3423, 0), new Tile(2975, 3423, 0), new Tile(2972, 3424, 0), new Tile(2969, 3424, 0), new Tile(2966, 3426, 0), new Tile(2963, 3426, 0), new Tile(2960, 3429, 0), new Tile(2957, 3432, 0), new Tile(2954, 3434, 0), new Tile(2953, 3437, 0), new Tile(2951, 3440, 0), new Tile(2949, 3443, 0), new Tile(2949, 3446, 0), new Tile(2948, 3449, 0)};
    //doric to goblin = public static final Tile[] path = {new Tile(2948, 3449, 0), new Tile(2948, 3452, 0), new Tile(2948, 3455, 0), new Tile(2948, 3458, 0), new Tile(2948, 3461, 0), new Tile(2948, 3464, 0), new Tile(2950, 3467, 0), new Tile(2952, 3470, 0), new Tile(2954, 3473, 0), new Tile(2954, 3476, 0), new Tile(2955, 3479, 0), new Tile(2955, 3482, 0), new Tile(2955, 3485, 0), new Tile(2955, 3488, 0), new Tile(2955, 3491, 0), new Tile(2956, 3494, 0), new Tile(2956, 3497, 0), new Tile(2956, 3500, 0), new Tile(2956, 3503, 0), new Tile(2956, 3506, 0), new Tile(2958, 3509, 0)};


    public PathGenerator()
    {

    }

    public Tile[] generatePath(Tile startTile, Tile endTile)
    {


        return null;
    }

}
